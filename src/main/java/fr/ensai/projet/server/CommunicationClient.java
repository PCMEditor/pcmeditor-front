package fr.ensai.projet.server;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;

import fr.ensai.projet.interfaceBackFront.PCMException;
import fr.ensai.projet.metier.Cell;
import fr.ensai.projet.metier.Comment;
import fr.ensai.projet.metier.PCM;

public class CommunicationClient {

	private static boolean initialise = false; //Enregistre si le Back est initialisé

	private static int lastPCMid = -1; //Enregistre l'identifiant de la dernière PCM utilisée
	private static PCM lastPCM = null; //Enregistre la dernière PCM utilisée

	/**
	 * Demande la liste des PCM au Back et génère le fichier XML correspondant
	 * @return le contenu du fichier XML généré
	 * @throws Exception
	 */
	public static String[][] getListePCM() throws Exception{
		String res[][] = null;
		if(initialisation()){
			HashMap<Integer, String> listePCM = CommunicationBack.getListePCM();
			MetierToGWTXML.generateXML(listePCM, "src/main/webapp/data/liste.xml");
			res = new String[listePCM.size()][2];
			int cpt=0;
			for(int id : listePCM.keySet()){
				res[cpt][0] = Integer.toString(id);
				res[cpt++][1] = listePCM.get(id);
			}
		}
		return(res);
	}

	/**
	 * Demande le nombre de matrices contenues dans une PCM
	 * @param id l'identifiant de la PCM concernée
	 * @return le nombre de matrices, -1 si la PCM n'existe pas
	 * @throws Exception
	 */
	public static int getNbMatPCM(int id) throws Exception{
		int res = -1;
		if(initialisation()){
			if(id != lastPCMid){
				lastPCM = CommunicationBack.getPCM(id);
			}
			if(lastPCM != null){
				res = lastPCM.getMatrices().size();
			}
		}
		return res;
	}

	/**
	 * Demande le contenu d'une matrice d'une PCM
	 * @param id identifiant de la matrice concernée
	 * @param numMat rang de la matrice concernée
	 * @return String contenant le nombre de colonnes, puis le nom de chaque colonne
	 * @throws Exception
	 */
	public static String getMatPCM(int id, int numMat) throws Exception{
		String res = "0";
		if(initialisation()){
			if(id != lastPCMid){
				lastPCM = CommunicationBack.getPCM(id);
			}
			if(lastPCM != null && lastPCM.getMatrices().size()>=numMat){
				List<String> colonnes = MetierToGWTXML.generateXML(lastPCM, numMat, "src/main/webapp/data/matrice.xml"); //TODO gérer le chemin du fichier XML généré
				res = Integer.toString(colonnes.size());
				for(String colonne : colonnes){
					res += ";" + colonne;
				}
			}
		}
		return res;
	}

	/**
	 * Demande si nécessaire l'initialisation du Back
	 * @return true si le serveur renoie une réponse HTTP 200 (succès), false sinon
	 * @throws Exception
	 */
	private static boolean initialisation() throws Exception{
		if(!initialise){
			initialise = CommunicationBack.init();
		}
		return(initialise);
	}

	/**
	 * Demande la liste des commentaires relatifs à une cellule d'une matrice
	 * @param id identifiant de la PCM
	 * @param numMat rang de la matrice
	 * @param row ligne
	 * @param col colonne
	 * @return tableau des noms d'auteur et des contenus de commentaires
	 * @throws Exception
	 */
	public static String[][] getCommentairesCell(int id, int numMat, int row, int col) throws Exception{
		String[][] res = null;
		if(initialisation()){
			if(id != lastPCMid){
				lastPCM = CommunicationBack.getPCM(id);
			}
			if(lastPCM != null){
				List<Comment> commentaires = lastPCM.getMatrices().get(numMat).getColmuns().get(col).getCells().get(row).getComments();
				res = new String[commentaires.size()][2];
				for (int i = 0; i < commentaires.size(); i++) {
					res[i][0] = commentaires.get(i).getAuthor();
					res[i][1] = commentaires.get(i).getContent();
				}
			}
		}
		return res;
	}

	/**
	 * Enregistre un nouveau commentaire et envoie la PCM modifiée au Back
	 * @param id identifiant de la PCM
	 * @param numMat rang de la matrice
	 * @param row ligne
	 * @param col colonne
	 * @param auteur nom de l'auteur
	 * @param commentaire contenu du commentaire
	 * @throws IOException
	 * @throws PCMException
	 * @throws Exception
	 */
	public static void addCommentaire(int id, int numMat, int row, int col,String auteur, String commentaire) throws IOException, PCMException, Exception{
		if(initialisation()){
			if(id != lastPCMid){
				lastPCM = CommunicationBack.getPCM(id);
			}
			if(lastPCM != null){
				@SuppressWarnings("rawtypes")
				Cell cell = lastPCM.getMatrices().get(numMat).getColmuns().get(col).getCells().get(row);
				cell.addComment(auteur, commentaire);
				CommunicationBack.sendPCM(id, lastPCM);
			}
		}
	}

}
