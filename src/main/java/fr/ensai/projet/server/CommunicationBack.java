package fr.ensai.projet.server;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.transform.stream.StreamSource;

import fr.ensai.projet.interfaceBackFront.PCMException;
import fr.ensai.projet.interfaceBackFront.XmlPCMExport;
import fr.ensai.projet.metier.PCM;

public class CommunicationBack {
	
	/**
	 * Adresse de la racine du serveur Back
	 */
	private static final String URL = "http://127.0.0.1:8080/PCMEditor/";

	/**
	 * Initialisation du serveur Back
	 * @return true si le serveur renvoie un code HTTP 200 (succès), false sinon
	 * @throws Exception
	 */
	public static boolean init() throws Exception {

		String url = URL + "pcm/init";

		URL obj = new URL(url);
		HttpURLConnection con = (HttpURLConnection) obj.openConnection();

		// optional default is GET
		con.setRequestMethod("GET");

		int responseCode = con.getResponseCode();
		System.out.println("\nSending 'GET' request to URL : " + url);
		System.out.println("Response Code : " + responseCode);

		return responseCode == 200;
	}

	/**
	 * Récupération de la liste des PCM disponibles dans le Back
	 * @return HashpMap, avec pour clé l'identifiant d'une PCM et pour valeur son nom
	 * @throws PCMException
	 * @throws IOException
	 */
	@SuppressWarnings({ "unchecked" })
	public static HashMap<Integer, String> getListePCM() throws PCMException, IOException {
		HashMap<Integer, String> listePCM = null;

		String url = URL + "pcm";

		URL obj = new URL(url);
		HttpURLConnection con = (HttpURLConnection) obj.openConnection();

		con.setRequestMethod("GET");

		int responseCode = con.getResponseCode();
		System.out.println("\nSending 'GET' request to URL : " + url);
		System.out.println("Response Code : " + responseCode);

		BufferedReader in = new BufferedReader(
				new InputStreamReader(con.getInputStream()));
		String inputLine;
		StringBuffer response = new StringBuffer();

		while ((inputLine = in.readLine()) != null) {
			response.append(inputLine);
		}
		in.close();

		JaxbHashMap<Integer, String> p = new JaxbHashMap<Integer, String>();
		JAXBContext jaxbContext;
		try {
			jaxbContext = JAXBContext.newInstance(JaxbHashMap.class);
			Unmarshaller jaxbunMarshaller = jaxbContext.createUnmarshaller();
			StreamSource ss = new StreamSource( new StringReader( response.toString() ) );
			p = (JaxbHashMap<Integer, String>) jaxbunMarshaller.unmarshal(ss);

			listePCM = p.constructRes();

		} catch (JAXBException e) {
			throw new PCMException("Erreur à l'import du fichier XML.");
		}

		return listePCM;
	}

	/**
	 * Récupération d'une PCM dans le Back
	 * @param id identifiant de la PCM
	 * @return la PCM correspondante
	 * @throws IOException
	 * @throws PCMException
	 */
	public static PCM getPCM (int id) throws IOException, PCMException{
		PCM res = null;

		String url = URL + "pcm/" + id;

		URL obj = new URL(url);
		HttpURLConnection con = (HttpURLConnection) obj.openConnection();

		con.setRequestMethod("GET");

		int responseCode = con.getResponseCode();
		System.out.println("\nSending 'GET' request to URL : " + url);
		System.out.println("Response Code : " + responseCode);

		if(responseCode == 201){
			BufferedReader in = new BufferedReader(
					new InputStreamReader(con.getInputStream()));
			String inputLine;
			StringBuffer response = new StringBuffer();

			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			in.close();

			JAXBContext jaxbContext;
			try {
				jaxbContext = JAXBContext.newInstance(PCM.class);
				Unmarshaller jaxbunMarshaller = jaxbContext.createUnmarshaller();
				StreamSource ss = new StreamSource( new StringReader( response.toString() ) );
				res = (PCM) jaxbunMarshaller.unmarshal(ss);

			} catch (JAXBException e) {
				throw new PCMException("Erreur à l'import du fichier XML.");
			}
		}
		return res;
	}
	
	/**
	 * Envoi d'une PCM vers le Back
	 * @param id identifiant de la PCM
	 * @param pcm PCM à envoyer
	 * @throws IOException
	 * @throws PCMException
	 */
	public static void sendPCM(int id, PCM pcm) throws IOException, PCMException{
		String url = URL + "pcm"; ///" + id;

		URL obj = new URL(url);
		HttpURLConnection con = (HttpURLConnection) obj.openConnection();
 
		con.setRequestMethod("PUT");
		con.setRequestProperty("Content-Type", "application/XML");
		
		String urlParameters = XmlPCMExport.export(pcm);
 
		// Send post request
		con.setDoOutput(true);
		DataOutputStream wr = new DataOutputStream(con.getOutputStream());
		wr.writeBytes(urlParameters);
		wr.flush();
		wr.close();
 
		int responseCode = con.getResponseCode();
		System.out.println("\nSending 'PUT' request to URL : " + url);
		System.out.println("Response Code : " + responseCode);
 
		BufferedReader in = new BufferedReader(
		        new InputStreamReader(con.getInputStream()));
		String inputLine;
		StringBuffer response = new StringBuffer();
 
		while ((inputLine = in.readLine()) != null) {
			response.append(inputLine);
		}
		in.close();
	}
}
