package fr.ensai.projet.server;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import fr.ensai.projet.metier.Colonne;
import fr.ensai.projet.metier.Matrix;
import fr.ensai.projet.metier.PCM;

public class MetierToGWTXML {

	/**
	 * Génération d'un fichier XML utilisable par GWT correspondant à une matrice d'une PCM
	 * @param pcm objet PCM concerné
	 * @param numMat rang de la matrice concernée
	 * @param fileLoc emplacement d'enregistrement du fichier
	 * @return la liste des noms de colonne
	 * @throws IOException
	 */
	public static List<String> generateXML (PCM pcm, int numMat, String fileLoc) throws IOException{
		List<String> res = new ArrayList<String>();
		List<Matrix> matrices = pcm.getMatrices();
		if (matrices.size()>numMat){
			String contentXML = "<response>\n\t<data>\n";
			Matrix matrice = matrices.get(numMat);
			List<Colonne> colonnes = matrice.getColmuns();

			int nRow = colonnes.get(0).getCells().size();
			int nCol = colonnes.size();

			for(int colonne = 0; colonne < nCol; colonne++){
				res.add((String) colonnes.get(colonne).getCells().get(0).getContent());
			}

			for(int ligne = 1; ligne < nRow; ligne++){
				contentXML += "\t\t<record>\n";
				for(int colonne = 0; colonne < nCol; colonne++){
					contentXML += "\t\t\t<col" + colonne + ">" + colonnes.get(colonne).getCells().get(ligne).getContent() + "</col" + colonne + ">\n";
				}
				contentXML += "\t\t</record>\n";
			}
			contentXML += "\t</data>\n</response>";

			// Ecriture du fichier
			File file = new File(fileLoc);
			if (!file.exists()) {
				file.createNewFile();
			}
			FileWriter fw = new FileWriter(file.getAbsoluteFile());
			BufferedWriter bw = new BufferedWriter(fw);
			bw.write(contentXML);
			bw.flush();
			bw.close();
		}
		return res;
	}

	/**
	 * Génération d'un fichier XML utilisable par GWT correspondant à toutes les matrices d'une PCM
	 * @param pcm objet PCM concerné
	 * @param fileloc répertoire d'enregistrement des fichiers
	 * @return liste des listes de noms de colonnes
	 * @throws IOException
	 */
	public static List<List<String>> generateXML (PCM pcm, String fileloc) throws IOException{
		List<List<String>> res = new ArrayList<List<String>>();
		List<String> temp;
		int numMat = 0;
		while((temp = generateXML(pcm, numMat++, fileloc + "/matrice"+numMat)).size()>0){
			System.out.println(numMat);
			res.add(temp);
		}
		return res;
	}

	/**
	 * Génération d'un fichier XML utilisable par GWT correspondant à toutes PCM dans le Back
	 * @param listePCM HashMap des identifiants et noms de PCM
	 * @param fileLoc emplacement d'enregistrement du fichier
	 * @return le contenu du fichier XML (en String)
	 * @throws IOException
	 */
	public static String generateXML (HashMap<Integer, String> listePCM, String fileLoc) throws IOException{
		String contentXML = "<response>\n\t<data>\n";

		for(int cle : new ArrayList<Integer>(listePCM.keySet())){
			contentXML += "\t\t<record>\n\t\t\t<name>"+listePCM.get(cle)+"</name>\n\t\t</record>\n";
		}
		contentXML += "\t</data>\n</response>";
		
		// Ecriture du fichier
		File file = new File(fileLoc);
		if (!file.exists()) {
			file.createNewFile();
		}
		FileWriter fw = new FileWriter(file.getAbsoluteFile());
		BufferedWriter bw = new BufferedWriter(fw);
		bw.write(contentXML);
		bw.flush();
		bw.close();
		return contentXML;
	}
}
