package fr.ensai.projet.server;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;

/**
 * 
 *L'implémentation de l'interface du RemoteService
 *
 */
public class PCMServiceImpl extends RemoteServiceServlet implements fr.ensai.projet.client.PCMService{


//	public String petitExemple(String s) {
//		return s+"42";
//	}

	
	/**
	 * Permet de renvoyer la liste des PCM
	 * 
	 */
	public String[][] getListePCM() {
		String[][] res = null;
		try {
			res = CommunicationClient.getListePCM();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return res;
	}

	/**
	 * Permet de renvoyer la liste des matrices pour un PCM donné
	 */
	public int getListeMatrice(int i) {
		int res = 0;
		try {
			res = CommunicationClient.getNbMatPCM(i);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return res;
	}

	/**
	 * Renvoie la matrice pour un PCM et une matrice donnés.
	 */
	public String getMatrice(int m, int pcm) {
		String res = "";
		try {
			res = CommunicationClient.getMatPCM(pcm, m);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return res;
	}
	
	
	/**
	 * Renvoie la liste des commentaires associés à une cellule (lxc), une matrice et un PCM donnés.
	 */
	public String[][] getCom(int m, int pcm, int l, int c) {
		String[][] res= new String[0][0];
		try {
			res = CommunicationClient.getCommentairesCell(pcm, m, l, c);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return res;
	}
	
}
