package fr.ensai.projet.client;

import java.util.LinkedHashMap;

import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.rpc.IncompatibleRemoteServiceException;
import com.google.gwt.user.client.rpc.InvocationException;
import com.smartgwt.client.data.DataSource;
import com.smartgwt.client.data.fields.DataSourceTextField;
import com.smartgwt.client.types.DSDataFormat;
import com.smartgwt.client.types.SelectionStyle;
import com.smartgwt.client.widgets.Canvas;
import com.smartgwt.client.widgets.IButton;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.ComboBoxItem;
import com.smartgwt.client.widgets.form.fields.TextAreaItem;
import com.smartgwt.client.widgets.form.fields.TextItem;
import com.smartgwt.client.widgets.form.fields.events.ChangedEvent;
import com.smartgwt.client.widgets.form.fields.events.ChangedHandler;
import com.smartgwt.client.widgets.grid.ListGrid;
import com.smartgwt.client.widgets.grid.ListGridRecord;
import com.smartgwt.client.widgets.grid.events.CellClickEvent;
import com.smartgwt.client.widgets.grid.events.CellClickHandler;
import com.smartgwt.client.widgets.layout.HLayout;
import com.smartgwt.client.widgets.layout.VLayout;

/**
 * Entry point classes define <code>onModuleLoad()</code>.
 */
public class projetgl implements EntryPoint {
	


	
	/**
	 * Code lancé au chargement du module
	 */
	public void onModuleLoad() {
		

		/////////// Sources à mettre dans les matrices, dans chaque onglet
		
		//DataSource en dur pour tester l'ajout de lignes...
				DataSource dataSource = new DataSource();  
				dataSource.setDataFormat(DSDataFormat.XML);  
				DataSourceTextField nameField = new DataSourceTextField("name", "Name");  
				DataSourceTextField emailField = new DataSourceTextField("email", "Email");  
				DataSourceTextField orgField = new DataSourceTextField("organization", "Organization");  
				DataSourceTextField phoneField = new DataSourceTextField("phone", "Phone");  
				DataSourceTextField streetField = new DataSourceTextField("street", "Street");  
				streetField.setValueXPath("address/street");  
				DataSourceTextField cityField = new DataSourceTextField("city", "City");  
				cityField.setValueXPath("address/city");  
				DataSourceTextField stateField = new DataSourceTextField("state", "State");  
				stateField.setValueXPath("address/statdatae");  
				DataSourceTextField zipField = new DataSourceTextField("zip", "Zip");  
				zipField.setValueXPath("address/zip");  
		
				dataSource.setFields(nameField, emailField, orgField, phoneField, streetField,cityField, stateField, zipField); 

		//////// Combo box

		final DynamicForm form = new DynamicForm();  
		form.setWidth(250);  
		form.setNumCols(1);
		form.setMargin(10);
		form.setHeight(50);

		final ComboBoxItem cbItem = new ComboBoxItem("ChoixMat");  
		cbItem.setTitle("Choix de matrice");  
		form.setItems(cbItem);

		
		//////// Stats

		final ListGrid statGrid = new ListGrid();  
		statGrid.setWidth(300);  
		statGrid.setHeight(200);  
		statGrid.setEmptyCellValue("--");
		statGrid.setAutoFetchData(true);

		//////Layout de boutons pour la sauvegarde et l'import

		final IButton stretchButton = new IButton("Sauvegarde");  
		stretchButton.setWidth(146);  
		stretchButton.setShowRollOver(true);  
		stretchButton.setShowDisabled(true);  
		stretchButton.setShowDown(true);  
		stretchButton.setTitleStyle("stretchTitle");

		final IButton stretchButton2 = new IButton("Import");  
		stretchButton2.setWidth(146); 
		stretchButton2.setShowRollOver(true);  
		stretchButton2.setShowDisabled(true);  
		stretchButton2.setShowDown(true);  
		stretchButton2.setTitleStyle("stretchTitle");

		//////Recherche de PCM

		Canvas canvas = new Canvas();  

		final ListGrid PCMItemGrid = new ListGrid();  
		PCMItemGrid.setWidth(300);  
		PCMItemGrid.setHeight(300);
		//On importe la liste des PCM dans la ListGrid
		getListPCM(PCMItemGrid);
		//supplyItemGrid.setAutoFetchData(true);  
		//supplyItemGrid.setShowFilterEditor(true);  
		//supplyItemGrid.setFilterOnKeypress(true);  
		//supplyItemGrid.setFetchDelay(500);  
		
		canvas.addChild(PCMItemGrid);  
		canvas.draw();  
		

		/* On ajout un clickHandler pour pouvoir mettre à jour la liste des matrices
		 * à chaque clic sur une nouvelle PCM.
		 */
		PCMItemGrid.addCellClickHandler(new CellClickHandler() {
			public void onCellClick(CellClickEvent event) {
				//PCM passé en dur... Mais le ComboBox est là
				getListMat(cbItem,getNumPCM());
			}
		});
		


		
		//////// Division en layout

		//Layout principal
		HLayout layout2 = new HLayout();  
		layout2.setWidth100();  
		layout2.setHeight100();  
		layout2.setMembersMargin(20);  

		//Layout de boutons
		HLayout buttonLayout = new HLayout();  
		buttonLayout.setMembersMargin(10);  
		buttonLayout.addMember(stretchButton);  
		buttonLayout.addMember(stretchButton2); 

		//Layout vertical pour les stat + boutons
		VLayout vLayout = new VLayout();  
		vLayout.addMember(canvas);
		vLayout.addMember(buttonLayout);
		vLayout.addMember(form);
		vLayout.setShowEdges(true);  
		vLayout.setWidth(150); 
		vLayout.setHeight(700); 
		vLayout.setMembersMargin(5);  
		vLayout.setLayoutMargin(20);  
		vLayout.addMember(statGrid); 

//		final Img img = new Img();
//		img.setWidth(200);
//		img.setHeight(200);
//		img.setSrc("/logoPCM.jpg");
//		img.draw();

//		vLayout.addMember(img);

		//ajout au layout principal   
		layout2.addMember(vLayout);  

		/////////// Grille

		final ListGrid grid;
		grid = new ListGrid();  
		grid.setWidth100();  
		grid.setHeight(700);  
		grid.setAutoFetchData(true); 
		grid.setCanSelectCells(true);
		grid.setSelectionType(SelectionStyle.MULTIPLE);
		grid.setShowAllRecords(true);
		grid.setCanDragSelect(true);  
		grid.setDataSource(dataSource);

		IButton buttonEdit = new IButton("Nouvelle Ligne");   
		buttonEdit.addClickHandler(new ClickHandler() {  
			public void onClick(ClickEvent event) {  
				grid.startEditingNew();  
			}  
		}); 
	
		/*
		 * On ajout un ChangedHandler pour mettre à jour la grid quand on sélectionne une matrice dans la ComboBox.
		 */
		cbItem.addChangedHandler(new ChangedHandler() {
			public void onChanged(ChangedEvent event) {
				//On prend le contenu String et on enlève le "matrice" pour récupérer le numéro
				getMatrice(grid,getNumMat(cbItem),getNumPCM()); //PCM passé en dur pour l'instant...
			}});
		
		

		VLayout ongletLayout = new VLayout();  
		ongletLayout.setMembersMargin(15);  
		ongletLayout.setHeight("90%");
		ongletLayout.setMargin(20);


		////// Layout de commentaires 

		VLayout messageLayout = new VLayout();  
		messageLayout.setWidth(250);  
		messageLayout.setHeight(700);  
		messageLayout.setBorder("1px solid #6a6a6a");  
		messageLayout.setMargin(20); 
		messageLayout.setLayoutMargin(5); 
		messageLayout.setMembersMargin(5); 


		final TextItem subjectItem = new TextItem();  
		subjectItem.setTitle("Nom");  
		subjectItem.setWidth("*");  
		subjectItem.setDefaultValue("Ex: Patrick, Roger"); 

		final Canvas textCanvas = new Canvas();  
		textCanvas.setPadding(5);  
		textCanvas.setHeight(1);  

		final DynamicForm formCom = new DynamicForm();  
		form.setNumCols(2);  
		form.setHeight("*");   

		final TextAreaItem messageItem = new TextAreaItem();  
		messageItem.setShowTitle(false);  
		messageItem.setColSpan(2);  
		messageItem.setHeight("*");  
		messageItem.setWidth("*");  
		messageItem.setLength(5000);  
		formCom.setFields(subjectItem, messageItem);  

		messageLayout.addMember(textCanvas);  
		messageLayout.addMember(formCom);       

		IButton sendButton = new IButton("Publier");  
		sendButton.addClickHandler(new ClickHandler() {  
			public void onClick(ClickEvent event) {  
				textCanvas.setPrefix("<b>"+subjectItem.getEnteredValue()+": </b>"); 
				String c = textCanvas.getContents()+" <BR> <BR> "+textCanvas.getPrefix() + messageItem.getEnteredValue();
				textCanvas.setContents(c); 
				messageItem.setValue("");

			}  
		});   
		messageLayout.addMember(sendButton);

		
		/* On ajout un clickHandler pour pouvoir mettre à jour les commentaires
		 * à chaque clic sur une nouvelle cellule.
		 */
		grid.addCellClickHandler(new CellClickHandler() {
			public void onCellClick(CellClickEvent event) {
				int selec = getNumMat(cbItem);
				// PCM passé en dur pour l'instant... Mais la ligne, la column, la matrice et le Canvas sont bien là...
				if(selec != -1){
					getCom(textCanvas,selec,getNumPCM(),event.getRowNum(),event.getColNum());
				}
			}
		});

		//Rassemblement du ListGrid et du bouton dans le layout central
		ongletLayout.addMember(grid);
		ongletLayout.addMember(buttonEdit);
		ongletLayout.draw(); 

		//ajout au layout principal
		layout2.addMember(ongletLayout);  
		layout2.addMember(messageLayout);
		layout2.draw(); 
	}


// Test pour prouver qu'on peut passer ndes String par appels asyncrones
// Pour le faire marcher il faut décommenter les fonctions dans PCMService, PCMServicAsync et PCMServiceImpl
//		public void PCMpetitExemple(final Canvas textCanvas) {
//			// Initialize the service proxy.
//			// Set up the callback object.
//			PCMServiceAsync charg = (PCMServiceAsync) GWT.create(PCMService.class);
//	
//			AsyncCallback callback = new AsyncCallback<String>(){
//				public void onFailure(Throwable arg0) {
//					Window.alert("RPC failure, essaye encore !");
//					try {
//						throw arg0;
//					} catch (IncompatibleRemoteServiceException e) {
//						Window.alert(e.getMessage() + "    this client is not compatible with the server; cleanup and refresh the browser ");
//					} catch (InvocationException e) {
//						Window.alert(e.getMessage()+"    the call didn't complete cleanly");
//						// the call didn't complete cleanly
//					} catch (Throwable e) {
//						Window.alert(e.getMessage()+ "    last resort");
//						// last resort -- a very unexpected exception
//					}
//				}
//	
//				public void onSuccess(String arg0) {
//					Window.alert("Réponse : "+arg0);
//				}
//			};
//			
//			charg.petitExemple("test", callback);
//	
//	
//		}



	/**
	 * 
	 * @param field Une grille 
	 * Créer un DataSource avec le fichier XML de la liste des PCM en provenence du serveur Tomcat en front
	 * Associe ce DataSource à la grille passée en paramètre
	 * 
	 */
	public void getListPCM(final ListGrid field) {
		PCMServiceAsync pCMSvc = GWT.create(PCMService.class);
		pCMSvc.getListePCM( new AsyncCallback<String[][]>(){
			public void onFailure(Throwable arg0) {
				Window.alert("RPC failure !");
				try {
					throw arg0;
				} catch (IncompatibleRemoteServiceException e) {

					// Client pas compatible
					Window.alert(e.getMessage());
				} catch (InvocationException e) {
					Window.alert(e.getMessage());
					// Appel pas terminé correctement
				} catch (Throwable e) {
					Window.alert(e.getMessage());
					//Dernier espoir...
				}
			}

			public void onSuccess(String[][] arg0) {
				listPCMDS ds = listPCMDS.getInstance();
				ds.setClientOnly(true);
				String[] name = new String[2];
				name[0] = "key";
				name[1] = "name";
				ListGridRecord rec = new ListGridRecord();
				for (int i = 0; i < arg0.length; i++) {
					rec = new ListGridRecord();
					for (int j = 0; j < arg0[0].length; j++) {
						rec.setAttribute(name[j],arg0[i][j]);
					}
					ds.addData(rec);
				}
				field.setDataSource(ds);
			}
		});
	}

	/**
	 * 
	 * @param cb
	 * @param numPCM id du pcm sélectionné
	 * Permet d'afficher dans le ComboBox cb la liste des matrices disponibles pour le PCM selectioné
	 * 
	 */
	public void getListMat(final ComboBoxItem cb, int numPCM) {
		PCMServiceAsync pCMSvc = GWT.create(PCMService.class);
		pCMSvc.getListeMatrice(numPCM, new AsyncCallback<Integer>(){

			@Override
			public void onFailure(Throwable arg0) {
				Window.alert("RPC failure !");
				try {
					throw arg0;
				} catch (IncompatibleRemoteServiceException e) {

					// Client pas compatible
					Window.alert(e.getMessage());
				} catch (InvocationException e) {
					Window.alert(e.getMessage());
					// Appel pas terminé correctement
				} catch (Throwable e) {
					Window.alert(e.getMessage());
					//Dernier espoir...
				}
			}

			@Override
			public void onSuccess(Integer arg0) {
				LinkedHashMap<String, String> valueMap = new LinkedHashMap<String, String>(); 
				for (int i = 0; i < arg0; i++) {
					valueMap.put("matrice"+i, "Matrice"+i); 
				}
				cb.setValueMap(valueMap);  
			}});
	}
	
	
	/**
	 * @param lg
	 * @param numMat
	 * @param numPCM
	 * Permet d'afficher dans la ListGrid la matrice de numéro numMat du PCM d'id numPCM
	 */
	public void getMatrice(final ListGrid lg, int numMat, int numPCM) {
		PCMServiceAsync pCMSvc = GWT.create(PCMService.class);
		pCMSvc.getMatrice(numMat, numPCM, new AsyncCallback<String>(){

			@Override
			public void onFailure(Throwable arg0) {
				Window.alert("RPC failure !");
				try {
					throw arg0;
				} catch (IncompatibleRemoteServiceException e) {

					// Client pas compatible
					Window.alert(e.getMessage());
				} catch (InvocationException e) {
					Window.alert(e.getMessage());
					// Appel pas terminé correctement
				} catch (Throwable e) {
					Window.alert(e.getMessage());
					//Dernier espoir...
				}
			}

			@Override
			public void onSuccess(String arg0) {
				DataSource ds = new DataSource();
				ds.setClientOnly(true);
				lg.setDataSource(ds);

			}});
	}
	
	/**
	 * 
	 * @param tc
	 * @param numMat
	 * @param numPCM
	 * @param l
	 * @param c
	 * Permet d'ajouter les commentaires de la cellule lxc de la matrice numero numMat du PCM d'id numPCM au Canvas de commentaire passé en paramètre
	 * 
	 */
	public void getCom(final Canvas tc, int numMat, int numPCM, int l, int c) {
		PCMServiceAsync pCMSvc = GWT.create(PCMService.class);
		pCMSvc.getCom(numMat, numPCM, l, c, new AsyncCallback<String[][]>(){

			@Override
			public void onFailure(Throwable arg0) {
				Window.alert("RPC failure !");
				try {
					throw arg0;
				} catch (IncompatibleRemoteServiceException e) {

					// Client pas compatible
					Window.alert(e.getMessage());
				} catch (InvocationException e) {
					Window.alert(e.getMessage());
					// Appel pas terminé correctement
				} catch (Throwable e) {
					Window.alert(e.getMessage());
					//Dernier espoir...
				}
			}

			@Override
			public void onSuccess(String[][] arg0) {
				String content="";
				for (int i = 0; i < arg0.length; i++) {
					for (int j = 0; j < arg0[i].length; j++) {
						content = content+arg0[i][j]+" ";
					}
					content = content + "\n";
				}
				tc.setContents(content);
			}});
	}
	
	/**
	 * 
	 * @param cb
	 * @return le numéro de la matrice courante sélectionnée dans le choix des matrices.
	 */
	private int getNumMat(ComboBoxItem cb){
		int res = -1;
		//On enlève la partie 'matrice' de la chaine de caractère pour récupérer le num de la matrice
		res = Integer.parseInt(cb.getValueAsString().substring(7));
		return res;
	}
	
	/**
	 * 
	 * @return l'id du PCM courant sélectionné dans la listGrid de PCM
	 */
	private int getNumPCM(){
		return 1;
	}
}