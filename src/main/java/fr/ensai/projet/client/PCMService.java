package fr.ensai.projet.client;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;
/**
 * 
 * Interface qui définit les fonctions du Remote Service
 *
 */
@RemoteServiceRelativePath("listePCM")
public interface PCMService extends RemoteService {
//	String petitExemple(String s);
	String[][] getListePCM();
	int getListeMatrice(int i);
	String getMatrice(int m, int pcm);
	String[][] getCom(int m, int pcm, int l, int c);
	
}
