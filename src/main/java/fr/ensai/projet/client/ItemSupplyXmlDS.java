package fr.ensai.projet.client;


import com.smartgwt.client.data.DataSource;  
import com.smartgwt.client.data.fields.DataSourceBooleanField;  
import com.smartgwt.client.data.fields.DataSourceDateField;  
import com.smartgwt.client.data.fields.DataSourceEnumField;  
import com.smartgwt.client.data.fields.DataSourceFloatField;  
import com.smartgwt.client.data.fields.DataSourceIntegerField;  
import com.smartgwt.client.data.fields.DataSourceTextField;  
import com.smartgwt.client.widgets.form.validator.FloatPrecisionValidator;  
import com.smartgwt.client.widgets.form.validator.FloatRangeValidator;  
  
public class ItemSupplyXmlDS extends DataSource {  
  
    private static ItemSupplyXmlDS instance = null;  
  
    public static ItemSupplyXmlDS getInstance() {  
        if (instance == null) {  
            instance = new ItemSupplyXmlDS("supplyItemDS");  
        }  
        return instance;  
    }  
  
    public ItemSupplyXmlDS(String id) {  
  
        setID(id);  
        setRecordXPath("/List/supplyItem");  
        DataSourceIntegerField pkField = new DataSourceIntegerField("id");  
        pkField.setHidden(true);  
        pkField.setPrimaryKey(true);
  
        DataSourceTextField nameField = new DataSourceTextField("namePCM", "Nom", 2000);  
  
        setFields(pkField, nameField);  
  
        setDataURL("ds/test_data/supplyItem.data.xml");  
        setClientOnly(true);          
    }  
}  