package fr.ensai.projet.client;



import com.google.gwt.user.client.rpc.AsyncCallback;

/**
 * 
 * Interface pour les appels asynchrones
 *
 */
public interface PCMServiceAsync {

//	void petitExemple(String s, AsyncCallback<String> callback);

	void getListePCM(AsyncCallback<String[][]> callback);

	void getListeMatrice(int i, AsyncCallback<Integer> callback);	
	
	void getMatrice(int m, int pcm, AsyncCallback<String> callback);

	void getCom(int m, int pcm, int l, int c, AsyncCallback<String[][]> callback);
}
