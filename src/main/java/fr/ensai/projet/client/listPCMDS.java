package fr.ensai.projet.client;

import com.smartgwt.client.data.DataSource;
import com.smartgwt.client.data.RestDataSource;
import com.smartgwt.client.data.fields.DataSourceTextField;
import com.smartgwt.client.types.DSDataFormat;
import com.smartgwt.client.widgets.grid.ListGridRecord;


	public class listPCMDS extends RestDataSource{

		private static listPCMDS instance = null;

		public static listPCMDS getInstance(){
			if(instance == null){
				instance = new listPCMDS("bonjour");
			}
			return instance;
		}

		public listPCMDS(String id){
			setID(id);
			//setDataFormat(DSDataFormat.XML);
			DataSourceTextField nameField = new DataSourceTextField("name", "PCM");  
			DataSourceTextField IdField = new DataSourceTextField("key", "ID");  
			setFields(IdField, nameField);
			//setDataURL("data/liste.xml");
//			ListGridRecord record = new ListGridRecord();
//			record.setAttribute("name", "test!!!");
//			addData(record);
		}
	}
