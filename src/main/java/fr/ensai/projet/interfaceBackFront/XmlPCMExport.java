package fr.ensai.projet.interfaceBackFront;

import java.io.File;
import java.io.StringWriter;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

import fr.ensai.projet.metier.PCM;

public class XmlPCMExport {

	/**
	 * Saves pcmExport as XML to a String
	 * 
	 * @param pcmExport
	 * @throws PCMException 
	 */

	public static String export(PCM pcmExport) throws PCMException {

		String res = "";

		try {
			StringWriter sw = new StringWriter();
			JAXBContext jaxbContext = JAXBContext.newInstance(PCM.class);
			Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
			jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true); 
			jaxbMarshaller.marshal(pcmExport, sw);
			res = sw.toString();
		} catch (JAXBException e) {
			throw new PCMException("Erreur à l'export du fichier XML.");
		}
		
		return res;
		
	}
	
	public void export(PCM pcmExport, File f) throws PCMException {
		
		try {
			JAXBContext jaxbContext = JAXBContext.newInstance(PCM.class);
			Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
			jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true); 
			jaxbMarshaller.marshal(pcmExport, f);
		} catch (JAXBException e) {
			throw new PCMException("Erreur à l'export du fichier XML.");
		}
		
	}

}
