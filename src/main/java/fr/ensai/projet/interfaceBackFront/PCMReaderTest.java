package fr.ensai.projet.interfaceBackFront;

import static org.junit.Assert.assertEquals;

import java.io.File;
import java.util.List;
import java.util.logging.Logger;

import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import fr.ensai.projet.metier.DataType;
import fr.ensai.projet.server.MetierToGWTXML;



/**
 * Test du lecteur de PCMMM
 * @author 
 *
 */
public class PCMReaderTest {
	
	@SuppressWarnings("unused")
	private static Logger _LOGGER = Logger.getLogger("PCMReaderTest");
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
		
	}
		
	
	@Test
	public void testImportEtExport(){
		File f = new File("testRecupMatNew.xml");
		File fexp = new File("testRecupMatNewExport.xml");
		XmlPCMImport imp = new XmlPCMImport();
		try {
		fr.ensai.projet.metier.PCM	p = imp.importer(f);
		assertEquals(p.getMatrices().get(0).getNameMatrix(),"General information");
		assertEquals(p.getMatrices().get(0).getColmuns().get(0).getCells().get(0).getContent(),"Application");
		assertEquals(p.getMatrices().get(0).getColmuns().get(0).getType(), DataType.Header);
		p.export(fexp);
			}catch(Exception e){
		e.printStackTrace();
			}
	}	
	
	@Test
	public void testGenXML(){
		File f = new File("testRecupMatNew.xml");
		XmlPCMImport imp = new XmlPCMImport();
		try {
		fr.ensai.projet.metier.PCM	p = imp.importer(f);
		List<List<String>> res = MetierToGWTXML.generateXML(p, "src/main/webapp/data/test.xml");
		for(List<String> temp : res){
			System.out.println(temp);
		}
			}catch(Exception e){
		e.printStackTrace();
			}
	}	
	
	
}