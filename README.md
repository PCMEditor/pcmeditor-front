# Manuel d'installation #

## Installation et configuration d'Eclipse ##
1. Installer Eclipse IDE for Java Developpers v4.4.1 (Luna)
2. Ouvrir le menu Help > Install New Software
3. Dans le champ "Work with", mettre "Luna - http://download.eclipse.org/releases/luna/"
4. Installer les add-ons suivants :
 
* Eclipse Git Team Provider (et Source Code)
* Eclipse Java EE Developer Tools
* Eclipse Java Web Developer Tools
* Eclipse Web Developer Tools
* Google Web Toolkit SDK 2.6.0
* GWT Designer Core
* GWT Designer Editor
* GWT Designer GPE
* JAX-WS DOM Tools
* JAX-WS Tools
* m2e-wtp
* Task focused interface for Eclipse Git Team Provider
* WidowBuilder XML Core


## Import du projet ##
1. Ouvrir le menu File > Import
2. Selectionner Git > Projects from Git
3. Selectionner Clone URI
4. Indiquer l'URI du projet
5. Selectionner la branche "master"
6. Choisir un dossier de destination
7. Choisir Import Existing Projects
8. Selectionner PCMEditor-Front et cliquer sur Finish



## Lancer l'application ##
1. Dans Eclipse, cliquer droit sur le projet, Maven > Update project
2. Ouvrir un terminal et acceder au dossier d'import git (étape 6 de l'import du projet)
3. Lancer la commande sudo mvn gwt:run-codeserver
4. Dans un nouvel onglet du terminal, lancer la commande sudo mvn gwt:run
5. Ouvrir http://localhost:9876/ (URL affichée dans le terminal appelé en 2)
6. Mettre en bookmark le lien "Dev Mode On"
7. Ouvrir http://localhost:8888/
8. Cliquer sur le lien "Dev Mode On" mis en favori en étape 6 puis cliquer sur Compile

